all: moat_v_pcawg_local moat_v_pcawg_global

moat_v_pcawg_local: moat_v_pcawg_local.cpp variant_permutation_v3.h variant_permutation_v3.cpp
	g++ -Wall -o moat_v_pcawg_local moat_v_pcawg_local.cpp variant_permutation_v3.cpp
	
moat_v_pcawg_global: moat_v_pcawg_global.cpp variant_permutation_v3.h variant_permutation_v3.cpp
	g++ -Wall -o moat_v_pcawg_global moat_v_pcawg_global.cpp variant_permutation_v3.cpp
