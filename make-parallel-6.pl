#!/usr/bin/perl
# use Parallel::ForkManager;

# Script for chopping up the relevant input files into chromosomes and farming
# them out to parallel processes

# Number of permutations
my $perm;

# Blacklist file
my $blacklist_file;

# WG FASTA directory
my $wg_fasta;

# Variant file
my $variant_file;

# Local (l) or global (g) version
my $flavor;

# CPU core count
# my $core_count;

# Chromosome to process
my $chrnum;

# Argument checking
if (scalar(@ARGV) != 6) {
	print "Usage: perl make-parallel.pl [# of permutations] [blacklist file] [FASTA directory] [variant file] [flavor] [chr]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
	$blacklist_file = shift(@ARGV);
	$wg_fasta = shift(@ARGV);
	$variant_file = shift(@ARGV);
	$flavor = shift(@ARGV);
	# $core_count = shift(@ARGV);
	$chrnum = shift(@ARGV);
}

my $exe;
if ($flavor == 'l') {
	$exe = "./moat_v_pcawg_local_pthread";
} elsif ($flavor == 'g') {
	$exe = "./moat_v_pcawg_global";
} else {
	print "Invalid option for flavor: $flavor. Must be either \'l\' or \'g\'. Exiting.\n";
	exit(1);
}

# Partition variants
my $chr;
if ($chrnum == 23) {
	$chr = "chrX";
} elsif ($chrnum == 24) {
	$chr = "chrY";
} elsif ($chrnum == 25) {
	$chr = "chrM";
} else {
	$chr = "chr".$chrnum;
}

# Set up master output
my $master_out = "all-out/".$chr;

# Set up output directories
system("mkdir -p $master_out");

open VFILE, "<$variant_file" or die "Can't open $variant_file: $!\n";
my @all_variants = <VFILE>;
close(VFILE);

my @variants = ();
for (my $l = 0; $l < scalar(@all_variants); $l++) {
	my @parts = split(/\t/, $all_variants[$l]);
	if ($parts[0] eq $chr) {
		push(@variants, $all_variants[$l]);
	}
}

# Partition blacklist file
open BFILE, "<$blacklist_file" or die "Can't open $blacklist_file: $!\n";
my $outfile = $master_out."/bl.txt";
open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
while (my $line = <BFILE>) {
	my @parts = split(/\t/, $line);
	if ($parts[0] eq $chr) {
		print OUTFILE $line;
	}
}
close(OUTFILE);

# Partition variant file
my $outfile = $master_out."/var.txt";
open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
for (my $j = 0; $j < scalar(@variants); $j++) {
	# chomp($variants[$j]);
	print OUTFILE $variants[$j];
}
# 	if ($i == $core_count) {
# 		for (my $j = $var_pointer; $j < scalar(@variants); $j++) {
# 			print OUTFILE $variants[$j];
# 		}
# 	}
close(OUTFILE);

my $this_bl = $master_out."/bl.txt";
my $this_var = $master_out."/var.txt";
my $this_out = $master_out;

if (!(-z $this_var)) {
	my $command = "$exe $perm $this_bl $wg_fasta $this_var $this_out";
	system($command);
}

# Set up combined output
# my $combined_out = "all-out/combined";
# system("mkdir -p $combined_out");
# for (my $i = 1; $i <= $core_count; $i++) {
# # 	my $chr;
# # 	if ($i == 23) {
# # 		$chr = "chrX";
# # 	} elsif ($i == 24) {
# # 		$chr = "chrY";
# # 	} elsif ($i == 25) {
# # 		$chr = "chrM";
# # 	} else {
# # 		$chr = "chr".$i;
# # 	}
# 
# 	my $this_out = "$master_out/".$i;
# 
# 	for (my $j = 1; $j <= $perm; $j++) {
# 		my $perm_file = "permutation_".$j.".txt";
# 		my $command;
# 		if ($i == 1) {
# 			$command = "cat $this_out/$perm_file > $combined_out/$perm_file";
# 		} else {
# 			$command = "cat $this_out/$perm_file >> $combined_out/$perm_file";
# 		}
# 		if (-e "$this_out/$perm_file") {
# 			system($command);
# 		}
# 	}
# }
exit();
