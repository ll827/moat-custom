#!/usr/bin/perl
use Parallel::ForkManager;

# Script for chopping up the relevant input files into chromosomes and farming
# them out to parallel processes

# Number of permutations
my $perm;

# Blacklist file
my $blacklist_file;

# WG FASTA directory
my $wg_fasta;

# Variant file
my $variant_file;

# Local (l) or global (g) version
my $flavor;

# CPU core count
my $core_count;

# Chromosome to process
my $chrnum;

# Argument checking
if (scalar(@ARGV) != 7) {
	print "Usage: perl make-parallel.pl [# of permutations] [blacklist file] [FASTA directory] [variant file] [flavor] [CPU core count] [chr]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
	$blacklist_file = shift(@ARGV);
	$wg_fasta = shift(@ARGV);
	$variant_file = shift(@ARGV);
	$flavor = shift(@ARGV);
	$core_count = shift(@ARGV);
	$chrnum = shift(@ARGV);
}

my $exe;
if ($flavor == 'l') {
	$exe = "./moat_v_pcawg_local";
} elsif ($flavor == 'g') {
	$exe = "./moat_v_pcawg_global";
} else {
	print "Invalid option for flavor: $flavor. Must be either \'l\' or \'g\'. Exiting.\n";
	exit(1);
}

# Set up master output
my $master_out = "all-out-".$chrnum;

# Set up output directories
system("mkdir -p $master_out");

open VFILE, "<$variant_file" or die "Can't open $variant_file: $!\n";
my @all_variants = <VFILE>;
close(VFILE);

# Partition variants
my $chr;
if ($chrnum == 23) {
	$chr = "chrX";
} elsif ($chrnum == 24) {
	$chr = "chrY";
} elsif ($chrnum == 25) {
	$chr = "chrM";
} else {
	$chr = "chr".$chrnum;
}

my @variants = ();
for (my $l = 0; $l < scalar(@all_variants); $l++) {
	my @parts = split(/\t/, $all_variants[$l]);
	if ($parts[0] eq $chr) {
		push(@variants, $all_variants[$l]);
	}
}

# Partition blacklist file
open BFILE, "<$blacklist_file" or die "Can't open $blacklist_file: $!\n";
my $outfile = $master_out."/bl.txt";
open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
while (my $line = <BFILE>) {
	my @parts = split(/\t/, $line);
	if ($parts[0] eq $chr) {
		print OUTFILE $line;
	}
}
close(OUTFILE);

my $unit_size = int(scalar(@variants)/$core_count)+1;
my $modulus = scalar(@variants) % $core_count;

my $var_pointer = 0;

for (my $i = 1; $i <= $core_count; $i++) {

	if ($i == $modulus+1) {
		$unit_size--;
	}

	# Output directory
	system("mkdir -p $master_out/$i");

	# Partition variant file
	my $outfile = "$master_out/".$i."/var.txt";
	open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
	for (my $j = $var_pointer; $j < $var_pointer+$unit_size; $j++) {
		# chomp($variants[$j]);
		print OUTFILE $variants[$j];
	}
	$var_pointer = $var_pointer+$unit_size;
# 	if ($i == $core_count) {
# 		for (my $j = $var_pointer; $j < scalar(@variants); $j++) {
# 			print OUTFILE $variants[$j];
# 		}
# 	}
	close(OUTFILE);

}

my $pm = new Parallel::ForkManager($core_count);
for (my $i = 1; $i <= $core_count; $i++) {
	$pm->start and next;

# 	my $chr;
# 	if ($i == 23) {
# 		$chr = "chrX";
# 	} elsif ($i == 24) {
# 		$chr = "chrY";
# 	} elsif ($i == 25) {
# 		$chr = "chrM";
# 	} else {
# 		$chr = "chr".$i;
# 	}

	my $this_bl = "$master_out/bl.txt";
	my $this_var = "$master_out/".$i."/var.txt";
	my $this_out = "$master_out/".$i;

	# if (!(-z $this_var)) {
		my $command = "$exe $perm $this_bl $wg_fasta $this_var $this_out";
		system($command);
	# }

	$pm->finish;
}
$pm->wait_all_children;

my @blacklist_varcount;
my @missed_trimer_varcount;
for (my $j = 1; $j <= $perm; $j++) {
	push(@blacklist_varcount, 0);
	push(@missed_trimer_varcount, 0);
}

# Set up combined output
my $combined_out = "$master_out/combined";
system("mkdir -p $combined_out");
for (my $i = 1; $i <= $core_count; $i++) {
# 	my $chr;
# 	if ($i == 23) {
# 		$chr = "chrX";
# 	} elsif ($i == 24) {
# 		$chr = "chrY";
# 	} elsif ($i == 25) {
# 		$chr = "chrM";
# 	} else {
# 		$chr = "chr".$i;
# 	}

	my $this_out = "$master_out/".$i;

	for (my $j = 1; $j <= $perm; $j++) {
		my $perm_file = "permutation_".$j.".txt";
		my $skip_file = "skipped_".$j.".txt";
		my $command;
		# my $command2;
		if ($i == 1) {
			$command = "cat $this_out/$perm_file > $combined_out/$perm_file";
			# $command2 = "cat $this_out/$skip_file > $combined_out/$skip_file";
		} else {
			$command = "cat $this_out/$perm_file >> $combined_out/$perm_file";
			# $command2 = "cat $this_out/$skip_file >> $combined_out/$skip_file";
		}
		if (-e "$this_out/$perm_file") {
			system($command);
		}
# 		if (-e "$this_out/$skip_file") {
# 			system($command2);
# 		}
		if (-e "$this_out/$skip_file") {
			open SFILE, "<$this_out/$skip_file"  or die "Can't open $this_out/$skip_file: $!\n";
			my $line = <SFILE>;
			chomp($line);
			my @pieces = split(/\s/, $line);
			$blacklist_varcount[$j] += $pieces[0];
		
			$line = <SFILE>;
			chomp($line);
			@pieces = split(/\s/, $line);
			$missed_trimer_varcount[$j] += $pieces[0];
			close(SFILE);
		}
	}
}

# Output combined skip files
for (my $i = 1; $i <= $perm; $i++) {
	my $skip_file = "skipped_".$i.".txt";
	open SFILE, ">$combined_out/$skip_file" or die "Can't open $combined_out/$skip_file: $!\n";
	print SFILE "$blacklist_varcount[$i] variants skipped due to blacklist intersection\n";
	print SFILE "$missed_trimer_varcount[$i] variants skipped due to lack of suitable trimers in permutation window\n";
	close(SFILE);
}
exit();
