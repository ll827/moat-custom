#!/bin/sh
#PBS -l nodes=1:ppn=8
#PBS -l mem=15GB
#PBS -m abe -M lucas.lochovsky@yale.edu
#PBS -N moat_v_pcawg_cluster
#PBS -r n
#PBS -j oe
#PBS -q gerstein
#PBS -V

cd /net/gerstein/ll426/code/moat-custom
# perl make-parallel-4.pl 1 /net/gerstein/ll426/larva/code/permutation-code/blacklist/combined.3col.sort.merge.bed /net/gerstein/ll426/wg-fasta /net/gerstein/ll426/larva/raw-data/variant-data/all-samples-pooled-variants/all-samples-pooled-variants.txt l 8
time ./moat_v_pcawg_local_pthread 1 /net/gerstein/ll426/larva/code/permutation-code/blacklist/combined.3col.sort.merge.bed /net/gerstein/ll426/wg-fasta /net/gerstein/ll426/larva/data/var10k.txt test/out
