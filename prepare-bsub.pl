#!/usr/bin/perl

# Write out separate qsub scripts for each chromosome

# Number of permutations
my $perm;

# Blacklist file
my $blacklist_file;

# WG FASTA directory
my $wg_fasta;

# Variant file
my $variant_file;

# Local (l) or global (g) version
my $flavor;

# CPUs per node
my $ncpu;

# Argument checking
if (scalar(@ARGV) != 6) {
	print "Usage: perl prepare-bsub.pl [# of permutations] [blacklist file] [FASTA directory] [variant file] [flavor] [ncpu]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
	$blacklist_file = shift(@ARGV);
	$wg_fasta = shift(@ARGV);
	$variant_file = shift(@ARGV);
	$flavor = shift(@ARGV);
	$ncpu = shift(@ARGV);
}

for (my $i = 1; $i <= 25; $i++) {
# for (my $i = 1; $i <= 1; $i++) {
	my $qsub_file = "bsub_".$i.".sh";
	
	my $qsub_header = "#!/bin/bash\n";
	$qsub_header .= "#BSUB -J moat_v_pcawg_cluster_".$i."\n";
	$qsub_header .= "#BSUB -R \"span[hosts=1]\"\n";
	$qsub_header .= "#BSUB -q gerstein\n";
	$qsub_header .= "#BSUB -n $ncpu\n\n";
	
# 	$qsub_header .= "#PBS -l nodes=1:ppn=8\n";
# 	$qsub_header .= "#PBS -l mem=15GB\n";
# 	$qsub_header .= "#PBS -m abe -M lucas.lochovsky\@yale.edu\n";
# 	$qsub_header .= "#PBS -N moat_v_pcawg_cluster_".$i."\n";
# 	$qsub_header .= "#PBS -r n\n";
# 	$qsub_header .= "#PBS -j oe\n";
# 	$qsub_header .= "#PBS -q gerstein\n";
# 	$qsub_header .= "#PBS -V\n\n";
	$qsub_header .= "cd /home/fas/gerstein/ll426/scratch/code/moat-custom\n";

	my $command = "perl make-parallel-5.pl $perm $blacklist_file $wg_fasta $variant_file $flavor $ncpu $i";
	$qsub_header .= $command;
	$qsub_header .= "\n";
	# system($command);
	
	open MYFILE, ">$qsub_file" or die "Can't open $qsub_file: $!\n";
	print MYFILE $qsub_header;
	close(MYFILE);
	
	$command = "bsub < $qsub_file";
	system($command);
}
exit();
