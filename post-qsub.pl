#!/usr/bin/perl

# Combine the results of several qsub scripts working on separate chromsomes into
# complete permutation files

# Number of permutations
my $perm;

# Argument checking
if (scalar(@ARGV) != 1) {
	print "Usage: perl post-qsub.pl [# of permutations]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
}

# Set up combined output
my $master_out = "all-out-combined";

# Set up output directories
system("mkdir -p $master_out");

my @blacklist_varcount;
my @missed_trimer_varcount;
for (my $j = 1; $j <= $perm; $j++) {
	push(@blacklist_varcount, 0);
	push(@missed_trimer_varcount, 0);
}

for (my $i = 1; $i <= 25; $i++) {
	my $source_dir = "all-out-".$i;
	for (my $j = 1; $j <= $perm; $j++) {
		my $perm_file = "permutation_".$j.".txt";
		my $skip_file = "skipped_".$j.".txt";
		my $command;
		# my $command2;
		if ($i == 1) {
			$command = "cat $source_dir/combined/$perm_file > $master_out/$perm_file";
			# $command2 = "cat $source_dir/combined/$skip_file > $master_out/$skip_file";
		} else {
			$command = "cat $source_dir/combined/$perm_file >> $master_out/$perm_file";
			# $command2 = "cat $source_dir/combined/$skip_file >> $master_out/$skip_file";
		}
		if (-e "$source_dir/combined/$perm_file") {
			system($command);
		}
# 		if (-e "$source_dir/combined/$skip_file") {
# 			system($command2);
# 		}
		if (-e "$source_dir/combined/$skip_file") {
			open SFILE, "<$source_dir/combined/$skip_file"  or die "Can't open $source_dir/combined/$skip_file: $!\n";
			my $line = <SFILE>;
			chomp($line);
			my @pieces = split(/\s/, $line);
			$blacklist_varcount[$j] += $pieces[0];
		
			$line = <SFILE>;
			chomp($line);
			@pieces = split(/\s/, $line);
			$missed_trimer_varcount[$j] += $pieces[0];
			close(SFILE);
		}
	}
	$command = "rm -rf $source_dir";
	system($command);
}

# Output combined skip files
for (my $i = 1; $i <= $perm; $i++) {
	my $skip_file = "skipped_".$i.".txt";
	open SFILE, ">$master_out/$skip_file" or die "Can't open $master_out/$skip_file: $!\n";
	print SFILE "$blacklist_varcount[$i] variants skipped due to blacklist intersection\n";
	print SFILE "$missed_trimer_varcount[$i] variants skipped due to lack of suitable trimers in permutation window\n";
	close(SFILE);
}

exit();
