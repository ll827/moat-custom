#!/usr/bin/perl

# Write out separate qsub scripts for each chromosome

# Number of permutations
my $perm;

# Blacklist file
my $blacklist_file;

# WG FASTA directory
my $wg_fasta;

# Variant file
my $variant_file;

# Local (l) or global (g) version
my $flavor;

# Argument checking
if (scalar(@ARGV) != 5) {
	print "Usage: perl make-parallel.pl [# of permutations] [blacklist file] [FASTA directory] [variant file] [flavor]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
	$blacklist_file = shift(@ARGV);
	$wg_fasta = shift(@ARGV);
	$variant_file = shift(@ARGV);
	$flavor = shift(@ARGV);
}

for (my $i = 1; $i <= 25; $i++) {
	my $qsub_file = "qsub_".$i.".sh";
	
	my $qsub_header = "#!/bin/sh\n";
	$qsub_header .= "#PBS -l nodes=1:ppn=8\n";
	$qsub_header .= "#PBS -l mem=15GB\n";
	$qsub_header .= "#PBS -m abe -M lucas.lochovsky\@yale.edu\n";
	$qsub_header .= "#PBS -N moat_v_pcawg_cluster_".$i."\n";
	$qsub_header .= "#PBS -r n\n";
	$qsub_header .= "#PBS -j oe\n";
	$qsub_header .= "#PBS -q gerstein\n";
	$qsub_header .= "#PBS -V\n\n";
	$qsub_header .= "cd /net/gerstein/ll426/code/moat-custom\n";

	my $command = "perl make-parallel-6.pl $perm $blacklist_file $wg_fasta $variant_file $flavor $i";
	$qsub_header .= $command;
	$qsub_header .= "\n";
	# system($command);
	
	open MYFILE, ">$qsub_file" or die "Can't open $qsub_file: $!\n";
	print MYFILE $qsub_header;
	close(MYFILE);
	
	$command = "qsub $qsub_file";
	system($command);
}
exit();
