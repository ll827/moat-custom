#!/usr/bin/perl
# use Parallel::ForkManager;

# Script for chopping up the relevant input files into chromosomes and farming
# them out to parallel processes

# Number of permutations
my $perm;

# Argument checking
if (scalar(@ARGV) != 1) {
	print "Usage: perl make-parallel-3.pl [# of permutations]\n";
	exit(1);
} else {
	$perm = shift(@ARGV);
}

# 
# # Blacklist file
# my $blacklist_file;
# 
# # WG FASTA directory
# my $wg_fasta;
# 
# # Variant file
# my $variant_file;
# 
# # Local (l) or global (g) version
# my $flavor;
# 

# 
# my $exe;
# if ($flavor == 'l') {
# 	$exe = "./moat_v_pcawg_local";
# } elsif ($flavor == 'g') {
# 	$exe = "./moat_v_pcawg_global";
# } else {
# 	print "Invalid option for flavor: $flavor. Must be either \'l\' or \'g\'. Exiting.\n";
# 	exit(1);
# }
# 
# # Set up output directories
# system("mkdir -p all-out");
# 
# for (my $i = 1; $i <= 25; $i++) {
# 	my $chr;
# 	if ($i == 23) {
# 		$chr = "chrX";
# 	} elsif ($i == 24) {
# 		$chr = "chrY";
# 	} elsif ($i == 25) {
# 		$chr = "chrM";
# 	} else {
# 		$chr = "chr".$i;
# 	}
# 	
# 	# Output directory
# 	system("mkdir -p all-out/$chr");
# 	
# 	# Partition variant file
# 	open VFILE, "<$variant_file" or die "Can't open $variant_file: $!\n";
# 	my $outfile = "all-out/".$chr."/var.txt";
# 	open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
# 	while (my $line = <VFILE>) {
# 		my @parts = split(/\t/, $line);
# 		if ($parts[0] eq $chr) {
# 			print OUTFILE $line;
# 		}
# 	}
# 	close(VFILE);
# 	close(OUTFILE);
# 	
# 	# Partition blacklist file
# 	open BFILE, "<$blacklist_file" or die "Can't open $blacklist_file: $!\n";
# 	my $outfile = "all-out/".$chr."/bl.txt";
# 	open OUTFILE, ">$outfile" or die "Can't open $outfile: $!\n";
# 	while (my $line = <BFILE>) {
# 		my @parts = split(/\t/, $line);
# 		if ($parts[0] eq $chr) {
# 			print OUTFILE $line;
# 		}
# 	}
# 	close(VFILE);
# 	close(OUTFILE);
# }
# 
# my $pm = new Parallel::ForkManager(25);
# for (my $i = 1; $i <= 25; $i++) {
# 	$pm->start and next;
# 	
# 	my $chr;
# 	if ($i == 23) {
# 		$chr = "chrX";
# 	} elsif ($i == 24) {
# 		$chr = "chrY";
# 	} elsif ($i == 25) {
# 		$chr = "chrM";
# 	} else {
# 		$chr = "chr".$i;
# 	}
# 	
# 	my $this_bl = "all-out/".$chr."/bl.txt";
# 	my $this_var = "all-out/".$chr."/var.txt";
# 	my $this_out = "all-out/".$chr;
# 	
# 	if (!(-z $this_var)) {
# 		my $command = "$exe $perm $this_bl $wg_fasta $this_var $this_out";
# 		system($command);
# 	}
# 	
# 	$pm->finish;
# }
# $pm->wait_all_children;

# Set up combined output
my $combined_out = "all-out/combined";
system("mkdir -p $combined_out");
for (my $i = 1; $i <= 25; $i++) {
	my $chr;
	if ($i == 23) {
		$chr = "chrX";
	} elsif ($i == 24) {
		$chr = "chrY";
	} elsif ($i == 25) {
		$chr = "chrM";
	} else {
		$chr = "chr".$i;
	}
	
	my $this_out = "all-out/".$chr;
	
	for (my $j = 1; $j <= $perm; $j++) {
		my $perm_file = "permutation_".$j.".txt";
		my $command;
		if ($i == 1) {
			$command = "cat $this_out/$perm_file > $combined_out/$perm_file";
		} else {
			$command = "cat $this_out/$perm_file >> $combined_out/$perm_file";
		}
		if (-e "$this_out/$perm_file") {
			system($command);
		}
	}
}
exit();
